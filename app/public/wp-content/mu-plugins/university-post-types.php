<?php
function university_post_types()
{
    // Events Posts types
        register_post_type('campuses', array(
        'show_in_rest' => true,
        'supports' => array(
            'title','editor','excerpt'
        ),
        'has_archive' => true,
        'public' => true,
        'labels' => array(
            'name' => 'Campuses',
            'add_new_item' => 'Add New Campus',
            'edit_item' => 'Edit Campus',
            'all_items' => 'All Campuses',
            'singular_name' => 'Campus'
        ),
        'menu_icon' => 'dashicons-location-alt'

    ));
    // Events Posts types
    register_post_type('events', array(
        'show_in_rest' => true,
        'supports' => array(
            'title','editor','excerpt'
        ),
        'has_archive' => true,
        'public' => true,
        'labels' => array(
            'name' => 'Events',
            'add_new_item' => 'Add New Event',
            'edit_item' => 'Edit Event',
            'all_items' => 'All Events',
            'singular_name' => 'Event'
        ),
        'menu_icon' => 'dashicons-calendar'

    ));
    
    // Programs Posts types
    register_post_type('programs', array(
        'show_in_rest' => true,
        'rewrite'=> array('slug'=>'programs'),
        'supports' => array(
            'title'
        ),
        'has_archive' => true,
        'public' => true,
        'labels' => array(
            'name' => 'Programs',
            'add_new_item' => 'Add New Program',
            'edit_item' => 'Edit Program',
            'all_items' => 'All Programs',
            'singular_name' => 'Program'
        ),
        'menu_icon' => 'dashicons-awards'

    ));
    // Professors Posts types
    register_post_type('professors', array(
        'show_in_rest' => true,
        'supports' => array(
            'title','editor','thumbnail'
        ),
        'public' => true,
        'labels' => array(
            'name' => 'Professors',
            'add_new_item' => 'Add New Professor',
            'edit_item' => 'Edit Professor',
            'all_items' => 'All Professors',
            'singular_name' => 'Professors'
        ),
        'menu_icon' => 'dashicons-welcome-learn-more'

    ));
}
add_action('init', 'university_post_types');
