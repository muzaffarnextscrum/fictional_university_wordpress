import $ from 'jquery';
class Search {
    //1. describe and create/initiate our Object
    constructor() {
        this.addSearchHTML();
        this.resultsDiv = $('#search-overlay__results');
        this.openButton = $('.js-search-trigger');
        this.closeButton = $('.search-overlay__close');
        this.searchOverlay = $('.search-overlay');
        this.searchField = $('#search-term');
        this.isOverlayOpen = false;
        this.typingTimer;
        this.previousValue;
        this.isSpinnerVisible = false;
        this.events();
    }
    // 2. events
    events() {
        this.openButton.on('click', this.openOverlay.bind(this));
        this.closeButton.on('click', this.closeOverlay.bind(this));
        $(document).on('keydown', this.keyDispatcher.bind(this));
        this.searchField.on('keyup', this.TypeLogic.bind(this));
    }

    // 3. methods function()
    // logic for wait input for completing his searching
    TypeLogic() {
        if (this.searchField.val() != this.previousValue) {
            clearTimeout(this.typingTimer);

            if (this.searchField.val()) {
                if (!this.isSpinnerVisible) {
                    this.resultsDiv.html('<div class="spinner-loader"></div>');
                }
                this.typingTimer = setTimeout(this.getResults.bind(this), 750);
                this.isSpinnerVisible = true;
            } else {
                this.resultsDiv.html('');
                this.isSpinnerVisible = false;
            }

        }

        this.previousValue = this.searchField.val();

    }

    // methods to send request to server and return results
    getResults() {
        $.getJSON(universityData.root_url + '/wp-json/university/v1/search?name=' + this.searchField.val(), (results) => {
            this.resultsDiv.html(`
                <div class="row">
                    <div class="one-third">
                            <h2 class="search-overlay__section-title">General Informations</h2>
                            ${results && results.generealInfo && results.generealInfo.length ? '<ul class="link-list min-list">' : "<p>No general information matches that search.</p>"}
                            ${results && results.generealInfo && results.generealInfo.map(item => `<li><a href="${item.permalink}">${item.title}</a> ${item.postType == "post" ? `by ${item.authorName}` : ""}</li>`).join("")}
                          ${results && results.generealInfo && results.generealInfo.length ? "</ul>" : ""}
                    </div>
                    <div class="one-third">
                        <h2 class="search-overlay__section-title">Programs</h2>
                        ${results.programs.length ? '<ul class="link-list min-list">' : `<p>No programs match that search. <a href="${universityData.root_url}/programs">View all programs</a></p>`}
              ${results.programs.map(item => `<li><a href="${item.permalink}">${item.title}</a></li>`).join("")}
            ${results.programs.length ? "</ul>" : ""}
                    </div>
                    <div class="one-third">
                        <h2 class="search-overlay__section-title">Campuses</h2>
                        ${results.campuses.length ? '<ul class="link-list min-list">' : `<p>No campuses match that search. <a href="${universityData.root_url}/campuses">View all campuses</a></p>`}
                        ${results.campuses.map(item => `<li><a href="${item.permalink}">${item.title}</a></li>`).join("")}
                      ${results.campuses.length ? "</ul>" : ""}
                    </div>
                    <div class="one-third">
                        <h2 class="search-overlay__section-title">Professors</h2>
                        ${results.professors.length ? '<ul class="professor-cards">' : `<p>No professors match that search.</p>`}
                        ${results.professors
                    .map(
                        item => `
                          <li class="professor-card__list-item">
                            <a class="professor-card" href="${item.permalink}">
                              <img class="professor-card__image" src="${item.image}">
                              <span class="professor-card__name">${item.title}</span>
                            </a>
                          </li>
                        `
                    )
                    .join("")}
                      ${results.professors.length ? "</ul>" : ""}
                    </div>
                    <div class="one-third">
                        <h2 class="search-overlay__section-title">Events</h2>
                        ${results.events.length ? "" : `<p>No events match that search. <a href="${universityData.root_url}/events">View all events</a></p>`}
              ${results.events
                    .map(
                        item => `
                <div class="event-summary">
                  <a class="event-summary__date t-center" href="${item.permalink}">
                    <span class="event-summary__month">${item.month}</span>
                    <span class="event-summary__day">${item.day}</span>  
                  </a>
                  <div class="event-summary__content">
                    <h5 class="event-summary__title headline headline--tiny"><a href="${item.permalink}">${item.title}</a></h5>
                    <p>${item.description} <a href="${item.permalink}" class="nu gray">Learn more</a></p>
                  </div>
                </div>
              `
                    )
                    .join("")}
                    </div>
                </div>
                `);
            this.isSpinnerVisible = false
        });

        // this.resultsDiv.html('we will get results here');
        // this.isSpinnerVisible = false;
        // $.when(
        //     $.getJSON(universityData.root_url + '/wp-json/wp/v2/posts?search=' + this.searchField.val()),
        //     $.getJSON(universityData.root_url + '/wp-json/wp/v2/pages?search=' + this.searchField.val())
        // ).then((posts, pages) => {
        //     var combinedResults = posts[0].concat(pages[0]);
        //     if (combinedResults && combinedResults.length) {
        //         this.resultsDiv.html(`
        //         <h2 class='search-overlay__section-title'>General Informations</h2>
        //         <ul class='link-list min-list'>
        //         ${combinedResults.map(item =>
        //             `<li> <a href='${item.link}'>${item.title.rendered}</a>  ${item.authorName ? 'by' + item.authorName : ''}</li>`
        //         ).join('')}
        //         </ul>
        //         `);
        //     } else {
        //         this.resultsDiv.html('No search result found');
        //     }
        // }, () => {
        //     this.resultsDiv.html('An unexpected error occurs, plz try again');
        // });
    }
    // methods for opening search from key s and closing from Esc key press
    keyDispatcher(e) {
        if (e.keyCode == 83 && !this.isOverlayOpen && !$('input textarea').is(':focus')) {
            this.openOverlay();
        }
        if (e.keyCode == 27 && this.isOverlayOpen) {
            this.closeOverlay();
        }
    }
    openOverlay() {
        this.searchOverlay.addClass('search-overlay--active');
        this.searchField.val('');
        setTimeout(() => {
            this.searchField.focus();
        }, 301);

        $('body').addClass('body-no-scroll');
        this.isOverlayOpen = true;
    }
    closeOverlay() {
        this.searchOverlay.removeClass('search-overlay--active');
        $('body').removeClass('body-no-scroll');
        this.isOverlayOpen = false;
    }
    addSearchHTML() {
        $('body').append(`
        <div class="search-overlay">
            <div class="search-overlay__top">
                <div class="container">
                <i class="fa fa-search search-overlay__icon" aria-hidden="true"></i>
                <input type="text" class="search-term" placeholder="what are you looking for?" id="search-term">
                <i class="fa fa-window-close search-overlay__close" aria-hidden="true"></i>
                </div>
            </div>
            <div class="container">
                <div id="search-overlay__results"></div>
            </div>
        </div>
        `);
    }
}
export default Search;