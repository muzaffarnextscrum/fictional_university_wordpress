<?php

get_header();
pageBanner(array(
    'title'=>'ALL Programs',
    'subtitle' =>'Currently offered programs',
    'photo'=>'https://images.unsplash.com/photo-1555911599-e70784b1e21d?ixlib=rb-1.2.1&q=99&fm=jpg&crop=entropy&cs=tinysrgb&w=2048&fit=max&ixid=eyJhcHBfaWQiOjcwOTV9'
));
?>



<div class="container container--narrow page-section">
    <ul class="link-list min-list">
    <?php
    while (have_posts()) {
        the_post();
        ?>
        <li><a href="<?php the_permalink() ?>"><?= the_title() ?></a></li>
        
    <?php }
    ?>
    </ul>
    <?php echo paginate_links(); ?>
</div>

<?php get_footer();

?>