<?php
// get 1 post
get_header();
while (have_posts()) {
    the_post();
?>
    <div class="page-banner">
        <div class="page-banner__bg-image" style="background-image: url(<?php echo get_theme_file_uri('/images/ocean.jpg') ?>);"></div>
        <div class="page-banner__content container container--narrow">
            <h1 class="page-banner__title"><?php the_title(); ?></h1>
            <div class="page-banner__intro">
                <p>DONT FORGET TO REPLACE ME LATER</p>
            </div>
        </div>
    </div>
    <div class="container container--narrow page-section">
        <div class="metabox metabox--position-up metabox--with-home-link">
            <p><a class="metabox__blog-home-link" href="<?= get_post_type_archive_link('campuses') ?>"><i class="fa fa-home" aria-hidden="true"></i> All Campuses</a> <span class="metabox__main"><?php the_title() ?></span></p>
        </div>
        <div class="generic-content">
            <?php the_content(); ?>
        </div>
        <div class="acf-map">
            <?php
            $mapLocation = get_field('map_location');
            ?>
            <div class="marker" data-lat=<?= $mapLocation['lat'] ?> data-lng=<?= $mapLocation['lng'] ?>>
                <h3><?php the_title(); ?></h3>
                <?= $mapLocation['address'] ?>
            </div>


        </div>
        <?php
        $related_programs = new WP_Query(array(
            'post_type' => 'programs',
            'order_by' => 'title',
            'order' => 'ASC',
            'meta_query' => array(

                array(
                    'key' => 'related_campus',
                    'compare' => 'LIKE',
                    'value' => get_the_ID(),
                )
            ),
            'posts_per_page' => -1
        ));
        if ($related_programs->have_posts()) {
        ?>
            <h2 class="headline headline--medium">Programs available at this campus</h2>
            <hr class="section-break">
            <ul class="min-list link-list">
                <?php
                while ($related_programs->have_posts()) {
                    $related_programs->the_post();

                ?>
                    <li>
                        <a href="<?= the_permalink() ?>">
                            <?= the_title() ?>
                        </a></li>
                <?php
                }
                echo '</ul>';
            }
            wp_reset_postdata();
            $today = date('Ymd');
            $homeEvents = new WP_Query(array(
                'post_type' => 'events',
                'meta_key' => 'event_date',
                'order_by' => 'meta_value_num',
                'order' => 'ASC',
                'meta_query' => array(
                    array(
                        'key' => 'event_date',
                        'compare' => '>=',
                        'value' => $today,
                        'type' => 'numeric',
                    ),
                    array(
                        'key' => 'related_programs',
                        'compare' => 'LIKE',
                        'value' => get_the_ID(),
                    )
                ),
                'posts_per_page' => 2
            ));
            if ($homeEvents->have_posts()) {
                ?>
                <h2 class="headline headline--medium">Upcoming <?= get_the_title() ?> Events</h2>
                <hr class="section-break">
            <?php
                while ($homeEvents->have_posts()) {
                    $homeEvents->the_post();
                    get_template_part('template-parts/content', get_post_type());
                }
            }
            ?>

    </div>
<?php
}
get_footer();
?>