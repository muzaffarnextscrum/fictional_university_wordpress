<?php

add_action('rest_api_init', 'universityRegisterSearch');

function universityRegisterSearch()
{
    register_rest_route('university/v1', 'search', array(
        'methods' => WP_REST_SERVER::READABLE,
        'callback' => 'universitySearchResults'
    ));
}
function universitySearchResults($data)
{
    $mainQuery = new WP_Query(array(
        'post_type' => array('post','pages','professors','programs','campuses','events'),
        's' => sanitize_text_field($data['name'])
    ));
    $results = array(
        'generealInfo'=>array(),
        'professors'=>array(),
        'programs'=>array(),
        'events'=>array(),
        'campuses'=>array(),
    );
    while ($mainQuery->have_posts()) {
        $mainQuery->the_post();
        if(get_post_type()=="post" || get_post_type()=="pages"){
            array_push($results['generealInfo'], array(
                'title' => get_the_title(),
                'permalink' => get_the_permalink(),
                'postType'=>get_post_type(),
                'authorName'=> get_the_author()
            ));
        }
        if(get_post_type()=="professors" ){
            array_push($results['professors'], array(
                'title' => get_the_title(),
                'permalink' => get_the_permalink(),
                'image' => get_the_post_thumbnail_url(0,'professorLandscape')
            ));
        }
        if(get_post_type()=="programs" ){
            array_push($results['programs'], array(
                'title' => get_the_title(),
                'permalink' => get_the_permalink(),
                'id'=>get_the_id()
            ));
        }
        if(get_post_type()=="events" ){
            $date = new DateTime(get_field('event_date'));
            $description = null;
            if (has_excerpt()) {
                $description = get_the_excerpt();
            } else {
                $description = wp_trim_words(get_the_content(), 15);
            }
            array_push($results['events'], array(
                'title' => get_the_title(),
                'permalink' => get_the_permalink(),
                'month'=> $date->format('M'),
                'day'=> $date->format('d'),
                'description'=>$description
            ));
        }
        if(get_post_type()=="campuses" ){
            array_push($results['campuses'], array(
                'title' => get_the_title(),
                'permalink' => get_the_permalink()
            ));
        }

    }
    if(!empty($results['programs'])){
        $programMetaQuery= array('relation'=>'OR');
        $relatedCampuses = get_field('related_campus');
        if($relatedCampuses){
            foreach($relatedCampuses as $campus){
                array_push($results['campuses'], array(
                    'title'=>get_the_title($campus),
                    'permalink'=>get_the_permalink($campus)
                ));
            }
        }
    foreach($results['programs'] as $item){
        array_push($programMetaQuery,array(
            'key'=>'related_programs',
            'compare'=>'LIKE',
            'value'=> $item['id']
        ));
    }
    $programRelationQuery= new WP_Query(array(
        'post_type'=>array('professors','events'),
        'meta_query'=>$programMetaQuery
        ),
    );
    while ($programRelationQuery->have_posts()) {
        $programRelationQuery->the_post();


        if(get_post_type()=="events" ){
            $date = new DateTime(get_field('event_date'));
            $description = null;
            if (has_excerpt()) {
                $description = get_the_excerpt();
            } else {
                $description = wp_trim_words(get_the_content(), 15);
            }
            array_push($results['events'], array(
                'title' => get_the_title(),
                'permalink' => get_the_permalink(),
                'month'=> $date->format('M'),
                'day'=> $date->format('d'),
                'description'=>$description
            ));
        }

        if(get_post_type()=="professors" ){
            array_push($results['professors'], array(
                'title' => get_the_title(),
                'permalink' => get_the_permalink(),
                'image' => get_the_post_thumbnail_url(0,'professorLandscape')
            ));
        }
    }
    }
    $results['events'] = array_values(array_unique($results['events'], SORT_REGULAR)); 
    $results['professors'] = array_values(array_unique($results['professors'], SORT_REGULAR));
    return $results;
}
