<!DOCTYPE html>
<html <?php language_attributes() ?>>

<head>
  <meta charset="<?= bloginfo('charset') ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php wp_head(); ?>
</head>

<body <?php body_class() ?>>
  <header class="site-header">
    <div class="container">
      <h1 class="school-logo-text float-left"><a href="<?= site_url() ?>"><strong>Fictional</strong> University</a></h1>
      <!-- <span class="js-search-trigger site-header__search-trigger"><i class="fa fa-search" aria-hidden="true"></i></span> -->
      <a href="<?=esc_url(site_url('/search'))?>" class="js-search-trigger site-header__search-trigger"><i class="fa fa-search" aria-hidden="true"></i></a>

      <i class="site-header__menu-trigger fa fa-bars" aria-hidden="true"></i>
      <div class="site-header__menu group">
        <nav class="main-navigation">
          <?php
          /**
           * creating dynamic menu           * 
           */
          //  wp_nav_menu(array(
          //   'theme_location' => 'headerMenuLocation'
          // )) 
          /**
           * ending dynamic menu           * 
           */
          ?>

          <ul class="min-list group">
            <li <?php if (is_page('about-us/') || wp_get_post_parent_id(0) == 13) echo 'class="current-menu-item"' ?>><a href="<?= site_url('/about-us') ?>">About Us</a></li>
            <li <?php if (get_post_type() == 'programs') echo 'class="current-menu-item"' ?>><a href="<?= site_url('/programs') ?>">Programs</a></li>
            <li <?php if (get_post_type() == 'events' || is_page('past-events')) echo 'class="current-menu-item"' ?>><a href="<?= site_url('/events') ?>">Events</a></li>
            <li <?php if (get_post_type() == 'campuses') echo 'class="current-menu-item"' ?>><a href="<?= get_post_type_archive_link('campuses') ?>">Campuses</a></li>
            <li <?php if (get_post_type() == 'post') echo 'class="current-menu-item"' ?>><a href="<?= site_url('/blogs') ?>">Blog</a></li>
          </ul>
        </nav>
        <div class="site-header__util">
          <a href="#" class="btn btn--small btn--orange float-left push-right">Login</a>
          <a href="#" class="btn btn--small  btn--dark-orange float-left">Sign Up</a>
          <a href="<?=esc_url(site_url('/search'))?>" class="search-trigger js-search-trigger"><i class="fa fa-search" aria-hidden="true"></i></a>
        </div>
      </div>
    </div>
  </header>