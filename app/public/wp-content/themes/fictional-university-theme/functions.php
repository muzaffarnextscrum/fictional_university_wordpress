<?php
require get_theme_file_path('includes/search-route.php');
function university_custom_rest(){
    register_rest_field('post','authorName', array(
        'get_callback'=> function(){
            return get_the_author();
        }
    ));
}
add_action('rest_api_init', 'university_custom_rest');
function pageBanner($args = NULL)
{
    // banner featured image for pages
    if (!$args['title']) {
        $args['title'] = get_the_title();
    }
    if (!$args['subtitle']) {
        $args['subtitle'] = get_field('page_banner_subtitle');
    }
    if (!$args['photo']) {
        if (get_field('page_banner_background_image') && !is_archive() && !is_home()) {
            $pageBannerImage = get_field('page_banner_background_image');
            $args['photo'] = $pageBannerImage['sizes']['pageBanner'];
        } else {
            $args['photo'] = get_theme_file_uri('/images/ocean.jpg');
        }
    } else {
        $args['photo'] = get_theme_file_uri('/images/ocean.jpg');
    }
?>
    <div class="page-banner">
        <div class="page-banner__bg-image" style="background-image: url(<?= $args['photo'] ?>);"></div>
        <div class="page-banner__content container container--narrow">
            <h1 class="page-banner__title"><?= $args['title'] ?></h1>
            <div class="page-banner__intro">
                <p><?= $args['subtitle']; ?></p>
            </div>
        </div>
    </div>
<?php
}
function university_files()
{

    wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
    wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_script('googleMap', '//maps.googleapis.com/maps/api/js?key=AIzaSyB9C0iVNLS5_igud0vkq-_7wQTtMbmRgeI', NULL, '1.0', true);
    if (strstr($_SERVER['SERVER_NAME'], 'fictioanluniversity.local')) {
        wp_enqueue_script('main-university-js', 'http://localhost:3000/bundled.js', NULL, '1.0', true);
    } else {
        wp_enqueue_script('our-vendor-js', get_theme_file_uri('/bundled-assets/vendors~scripts.9678b4003190d41dd438.js'), NULL, '1.0', true);
        wp_enqueue_script('main-university-js', get_theme_file_uri('/bundled-assets/scripts.fdb04865c6452b6f3a26.js'), NULL, '1.0', true);
        wp_enqueue_style('university_main_styles', get_stylesheet_uri('/bundled-assets/styles.fdb04865c6452b6f3a26.css'));
    }
    wp_localize_script('main-university-js','universityData', array(
        'root_url'=>get_site_url()
    ));
}
add_action('wp_enqueue_scripts', 'university_files');

function university_features()
{
    /* creating Dynamic menus */
    register_nav_menu('headerMenuLocation', 'Header Menu Location');
    register_nav_menu('footerLocationOne', 'Footer Location One');
    register_nav_menu('footerLocationTwo', 'Footer Location Two');
    /* ending Dynamic menus */
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_image_size('professorLandscape', 400, 260, true);
    add_image_size('professorPortrait', 480, 650, true);
    add_image_size('pageBanner', 1500, 350, true);
}

add_action('after_setup_theme', 'university_features');

function adjust_university_queries($query)
{
    if (!is_admin() && is_post_type_archive('campuses') && $query->is_main_query()) {
        $query->set('posts_per_page', -1);
    }
    if (!is_admin() && is_post_type_archive('programs') && is_main_query()) {
        $query->set('order_by', 'title');
        $query->set('order', 'DESC');
        $query->set('posts_per_page', -1);
    }
    if (!is_admin() && is_post_type_archive('events') && is_main_query()) {
        $today = date('Ymd');
        $query->set('meta_key', 'event_date');
        $query->set('order_by', 'meta_value_num');
        $query->set('order', 'ASC');
        $query->set(
            'meta_query',
            array(
                array(
                    'key' => 'event_date',
                    'compare' => '>=',
                    'value' => $today,
                    'type' => 'numeric',
                )
            ),
        );
    }
}
add_action('pre_get_posts', 'adjust_university_queries');

function universityMapKey($api)
{
    $api['key'] = 'AIzaSyB9C0iVNLS5_igud0vkq-_7wQTtMbmRgeI';
    return $api;
}
add_action('acf/fields/google_map/api', 'universityMapKey');
