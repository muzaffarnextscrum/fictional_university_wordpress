<?php

get_header();
pageBanner(array(
    'title'=>'ALL Events',
    'subtitle' =>'See Whats going on in world',
    'photo'=>'https://images.unsplash.com/photo-1555911599-e70784b1e21d?ixlib=rb-1.2.1&q=99&fm=jpg&crop=entropy&cs=tinysrgb&w=2048&fit=max&ixid=eyJhcHBfaWQiOjcwOTV9'
));
 ?>



<div class="container container--narrow page-section">
    <?php
    while (have_posts()) {
        the_post();
        get_template_part('template-parts/content', get_post_type());    
    }
    ?>
    <?php echo paginate_links(); ?>
    <hr class="section-break">
    <p>Looking for recap of our past events? <a href="<?=site_url('past-events')?>">Checkout our past events</a></p>
</div>

<?php get_footer();

?>