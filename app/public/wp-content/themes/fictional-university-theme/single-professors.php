<?php
// get 1 post
get_header();
while (have_posts()) {
    the_post();
    pageBanner();
?>
    
    <div class="container container--narrow page-section">
        <!-- <div class="metabox metabox--position-up metabox--with-home-link">
            <p><a class="metabox__blog-home-link" href="<?= site_url('/events') ?>"><i class="fa fa-home" aria-hidden="true"></i> Events Home</a> <span class="metabox__main"><?php the_title() ?></span></p>
        </div> -->
        <div class="generic-content">
            <div class="row group">
                <div class="one-third">
                <?php the_post_thumbnail('professorPortrait'); ?>
                </div>
                <div class="two-thirds">
                    <?php the_content(); ?>
                </div>
            </div>

        </div>
        <hr class="section-break">
        <?php
        $related_programs = get_field('related_programs');
        if ($related_programs) { ?>
            <h2 class="headline headline--medium headline--post-title">Subject(s) Taught</h2>
            <ul class="link-list min-list">
                <?php
                foreach ($related_programs as $program) {
                ?>
                    <li><a href="<?= get_the_permalink($program) ?>"><?= get_the_title($program) ?></a></li>
            <?php
                }
            } ?>
            </ul>
    </div>
<?php
}
get_footer();
?>