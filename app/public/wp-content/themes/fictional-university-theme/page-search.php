<?php
// get 1 page
get_header();
while (have_posts()) {
    the_post();
    pageBanner(
        array(
            'subtitle' => 'Your searched for'.$_POST['s']?$_POST['s']:'',
            'photo' => 'https://images.unsplash.com/photo-1555911599-e70784b1e21d?ixlib=rb-1.2.1&q=99&fm=jpg&crop=entropy&cs=tinysrgb&w=2048&fit=max&ixid=eyJhcHBfaWQiOjcwOTV9'
        )
    );
?>


    <div class="container container--narrow page-section">
        <?php
        $theParent = wp_get_post_parent_id(get_the_ID());
        if ($theParent) { ?>
            <div class="metabox metabox--position-up metabox--with-home-link">
                <p><a class="metabox__blog-home-link" href="<?= get_permalink($theParent) ?>"><i class="fa fa-home" aria-hidden="true"></i> Back to <?= get_the_title($theParent) ?></a> <span class="metabox__main"><?php echo the_title(); ?></span></p>
            </div>
        <?php } ?>


        <?php
        $checkPages = get_pages(array(
            'child_of' => get_the_ID(),
        ));
        if ($theParent || $checkPages) { ?>
            <div class="page-links">
                <h2 class="page-links__title"><a href="<?= get_permalink($theParent) ?>"><?= get_the_title($theParent) ?></a></h2>

                <ul class="min-list">
                    <?php
                    if ($theParent) {
                        $findChildof = $theParent;
                    } else {
                        $findChildof = get_the_ID();
                    }
                    wp_list_pages(array(
                        'title_li' => NULL,
                        'child_of' => $findChildof,
                        'sort_column' => 'menu_order'
                    ));
                    ?>
                    <!-- <li class="current_page_item"><a href="#">Our History</a></li>
        <li><a href="#">Our Goals</a></li> -->
                </ul>
            </div>

        <?php } ?>
        <div class="generic-content">
            <form class="search-form" action="<?= esc_url(site_url('/')) ?>" method="post">
                <label class="headline headline--medium" for="s">Perform new search</label>
                <div class="search-form-row">
                    <input class="s" placeholder="what are you looking for" type="text" name="s" id="s">
                    <input class="search-submit" type="submit" value="Search">
                </div>

            </form>
        </div>

    </div>
<?php
}
get_footer();
?>