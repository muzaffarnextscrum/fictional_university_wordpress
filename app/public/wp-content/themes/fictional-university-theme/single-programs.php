<?php
// get 1 post
get_header();
while (have_posts()) {
    the_post();
?>
    <div class="page-banner">
        <div class="page-banner__bg-image" style="background-image: url(<?php echo get_theme_file_uri('/images/ocean.jpg') ?>);"></div>
        <div class="page-banner__content container container--narrow">
            <h1 class="page-banner__title"><?php the_title(); ?></h1>
            <div class="page-banner__intro">
                <p>DONT FORGET TO REPLACE ME LATER</p>
            </div>
        </div>
    </div>
    <div class="container container--narrow page-section">
        <div class="metabox metabox--position-up metabox--with-home-link">
            <p><a class="metabox__blog-home-link" href="<?= get_post_type_archive_link('programs') ?>"><i class="fa fa-home" aria-hidden="true"></i> All Programs</a> <span class="metabox__main"><?php the_title() ?></span></p>
        </div>
        <div class="generic-content">
            <?php the_field('main_body_content'); ?>
        </div>
        <?php
        $related_professors = new WP_Query(array(
            'post_type' => 'professors',
            'order_by' => 'title',
            'order' => 'ASC',
            'meta_query' => array(

                array(
                    'key' => 'related_programs',
                    'compare' => 'LIKE',
                    'value' => get_the_ID(),
                )
            ),
            'posts_per_page' => -1
        ));
        if ($related_professors->have_posts()) {
        ?>
            <h2 class="headline headline--medium"><?= get_the_title() ?> Professors</h2>
            <hr class="section-break">
            <ul class="professor-cards">
                <?php
                while ($related_professors->have_posts()) {
                    $related_professors->the_post();
                    $date = new DateTime(get_field('event_date'))
                ?>
                    <li class="professor-card__list-item">
                        <a href="<?= the_permalink() ?>" class="professor-card">
                            <img src="<?= the_post_thumbnail_url('professorLandscape') ?>" alt="" class="professor-card__image">
                            <span class="professor-card__name"><?= the_title() ?></span>
                        </a></li>
                <?php
                }
                echo '</ul>';
            }
            wp_reset_postdata();
            $today = date('Ymd');
            $homeEvents = new WP_Query(array(
                'post_type' => 'events',
                'meta_key' => 'event_date',
                'order_by' => 'meta_value_num',
                'order' => 'ASC',
                'meta_query' => array(
                    array(
                        'key' => 'event_date',
                        'compare' => '>=',
                        'value' => $today,
                        'type' => 'numeric',
                    ),
                    array(
                        'key' => 'related_programs',
                        'compare' => 'LIKE',
                        'value' => get_the_ID(),
                    )
                ),
                'posts_per_page' => 2
            ));
            if ($homeEvents->have_posts()) {
                ?>
                <h2 class="headline headline--medium">Upcoming <?= get_the_title() ?> Events</h2>
                <hr class="section-break">
                <?php
                while ($homeEvents->have_posts()) {
                    $homeEvents->the_post();
                    get_template_part('template-parts/content', get_post_type());
                }
            }
            wp_reset_postdata();
            $related_campuses = get_field('related_campus');
            if ($related_campuses) {
                ?>
                <hr class="section-break">
                <h2 class="headline headline--medium"><?= get_the_title() ?> is available in given Campuses</h2>

                <ul class="min-list link-list">
                    <?php
                    foreach ($related_campuses as $campus) {
                    ?>
                        <li>
                            <a href="<?= get_the_permalink($campus) ?>">
                                <?= get_the_title($campus) ?>
                            </a></li>
                <?php
                    }
                    echo '</ul>';
                }
                ?>

    </div>
<?php
}
get_footer();
?>