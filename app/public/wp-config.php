<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'CH4+/8dHLm8363oewfMydXksVG15+qvisX5DRc9IbCD/qwg3zFXjwah8wvUcXrUm3mTmnUgnMaqwJq6IQEo4EQ==');
define('SECURE_AUTH_KEY',  'hQPAcLviv+wo8mkUZXczL8YiV7FyPPNz3buVy9/hR+6abSYw0xpefXtw1UYoQiWWFJaAaszSm401OFYpe1CAUw==');
define('LOGGED_IN_KEY',    'hDmwgIindmx2SX0H/Y2/4zy265t4EFpyCr/9LF0n+89GjEDbKjvbL3ejT7n7gxPuXU+9mKxRLNDUfMBPJG+Dww==');
define('NONCE_KEY',        'rbKRsfpF4Lq47wdAC70JYSjRCKnuR5tO3Qce1u0pdb2aQpdKTcg8dIIQVLbKQaysjCiebDpwetwQPX5TCMhZoQ==');
define('AUTH_SALT',        '1IYLdScRSOaelGjl5ueMpGUQVHW/QI+HkbuKddDmo3fGJAV6kwyOGahEZm3cmpa6ZUSfkz73LKd/h1DG9O6EhA==');
define('SECURE_AUTH_SALT', 'tREpB2lBBB2IbhDeTvdHFDwtHhliXnWsMn/JDfG1YGUrHf9RM57SsqrQZozDNVF/XnL6Hghq2N0iRl0kEuHVXw==');
define('LOGGED_IN_SALT',   'FqPHuDB897diNJxoX1ZbBxYb2hxIZaIlinSd2pg9pZmhM+zW1XFFPSSWEUEE9SOX9MhyWwr1vyK7mrDEBS/NIA==');
define('NONCE_SALT',       '3IsOo1WQU6LEjO7j27CwbfmmlWiMI3UexRklPBPRpsrTc4XH4j/GZ3qcPwtxf6BKn+aqvQntbmUo/0PXj0a+vg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
